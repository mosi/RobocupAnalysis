# RobocupAnalysis

## Synopsis

This project contains code to analyze tactics in RoboCup 2D Soccer simulations, developed at the [Modeling and Simulation Group](https://mosi.informatik.uni-rostock.de).

## Publications

For more details please refer to the following publication:

* Tom Warnke and Adelinde M. Uhrmacher (2016): [Spatiotemporal Pattern Matching in RoboCup](http://dx.doi.org/10.1007/978-3-319-45889-2_7). - In: Multiagent System Technologies (MATES) 2016.

## Usage

The class `analyzer.Analyzer` contains some usage examples. The basic process is:

* Generate data from a RoboCup 2D soccer match. This happens in the `robocup.server` package.
    1. Start instances of the soccer server and two teams.
    2. Register as an observer of the server
    3. Wait for the match to finish
* The observer receives the positions of all players and the ball at each step and forwards these information to a message handler. Several message handler implementations are provided. Most notably, two implementations create a data graph for subsequent analysis:
    * The package `graph` contains a simple implementation of an in-memory graph including (de-)serialization for hard drive storage.
    * The package `neo4j` contains an implementation that uses a Neo4j graph database.
* Finally, both kinds of graph can be queried to find subgraph patterns that represent relevant tactical patterns.
    * The class `graph.alg.UllmannsAlgorithm` contains an implementation of Ullmann's subgraph isomorphism algorithm, tuned for the generated graphs.
    * Neo4j Cypher queries can be run in the `neo4j.Neo4jGraphQueryManager`
  
## Contributors

* [Tom Warnke](https://www.informatik.uni-rostock.de/~tw250/) (<tom.warnke@uni-rostock.de>)

## License

This project is distributed under the terms of the Apache 2.0 license.

## Dependencies/Acknowledgements

* Neo4j (https://www.neo4j.com)
* ANTLR (https://github.com/antlr/antlr4)
* Atan (https://github.com/robocup-atan/atan)