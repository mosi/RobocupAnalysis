/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph;

import java.io.*;

/**
 * Created by Tom
 */
public class GraphPersistence {

    private GraphPersistence() {
    }

    public static Graph readGraph(String file) {

        Graph graph = new GraphImpl();

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), "utf-8"))) {

            String line;

            while ((line = reader.readLine()) != null) {

                if (line.startsWith("Node")) {
                    NodeImpl.fromString(line, graph);
                } else if (line.startsWith("Edge")) {
                    EdgeImpl.fromString(line, graph);
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return graph;
    }

    public static void writeGraph(Graph graph, String file) {

        try (Writer writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(file), "utf-8"))) {

            for (Node node : graph.getNodes()) {
                writer.write(node.toString() + "\n");
            }

            for (Edge edge : graph.getEdges()) {
                writer.write(edge.toString() + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
