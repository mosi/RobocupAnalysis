/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Tom
 */
class NodeImpl implements Node {

    private final NodeType type;

    private final Map<String, Object> attributes = new HashMap<>();

    private final String id;

    private final Set<Edge> inEdges = new HashSet<>();

    private final Set<Edge> outEdges = new HashSet<>();

    public NodeImpl(NodeType type, String id) {
        this.type = type;
        this.id = id;
    }

    public static Node fromString(String string, Graph graph) {

        if (string.startsWith("Node={") && string.endsWith("}")) {

            String[] data = string.substring(6, string.length() - 1).split(",");

            if (data.length >= 2) {

                String id = data[0];
                NodeType type = NodeType.valueOf(data[1]);

                Node node = graph.newNode(type, id);

                for (int i = 2; i < data.length; i = i + 2) {
                    node.setAttribute(data[i], data[i + 1]);
                }

                return node;
            }
        }

        return null;
    }

    @Override
    public NodeType getType() {
        return type;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Set<Edge> getInEdges() {
        return inEdges;
    }

    @Override
    public Set<Edge> getOutEdges() {
        return outEdges;
    }

    @Override
    public void addInEdge(Edge edge) {
        inEdges.add(edge);
    }

    @Override
    public void addOutEdge(Edge edge) {
        outEdges.add(edge);
    }

    @Override
    public Object getAttribute(String key) {
        if (key.equals("id")) {
            return id;
        }
        return attributes.get(key);
    }

    @Override
    public Object setAttribute(String key, Object value) {
        return attributes.put(key, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        NodeImpl node = (NodeImpl) o;

        if (type != node.type)
            return false;
        return id.equals(node.id);

    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Node={").append(id).append(',').append(type);

        if (!attributes.isEmpty()) {
            attributes.forEach(
                    (s, o) -> builder.append(',').append(s).append(',').append(o));
        }

        builder.append('}');

        return builder.toString();
    }
}
