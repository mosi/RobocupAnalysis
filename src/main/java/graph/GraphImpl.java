/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph;

import java.util.*;

/**
 * Created by Tom
 */
class GraphImpl implements Graph {

    private Set<Node> nodes = new HashSet<>();

    private Map<NodeType, Map<String, Node>> index = new HashMap<>();

    private Set<Edge> edges = new HashSet<>();

    public GraphImpl() {
        index.put(NodeType.LOCATION, new HashMap<>());
        index.put(NodeType.OBJECT, new HashMap<>());
        index.put(NodeType.TIMEPOINT, new HashMap<>());
    }

    @Override
    public Node newNode(NodeType type, String id) {
        Node node = new NodeImpl(type, id);
        nodes.add(node);
        index.get(node.getType()).put(id, node);
        return node;
    }

    @Override
    public Edge newEdge(EdgeType type, Node start, Node end) {
        Edge edge = new EdgeImpl(type, start, end);
        edges.add(edge);
        return edge;
    }

    @Override
    public Node getNode(NodeType type, String id) {
        return index.get(type).get(id);
    }

    @Override
    public Set<Node> getNodes() {
        return nodes;
    }

    @Override
    public Set<Node> getNodes(NodeType type) {
        return new HashSet<>(index.get(type).values());
    }

    @Override
    public Set<Edge> getEdges() {
        return edges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        GraphImpl graph = (GraphImpl) o;
        return Objects.equals(nodes, graph.nodes) && Objects
                .equals(edges, graph.edges);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodes, edges);
    }
}
