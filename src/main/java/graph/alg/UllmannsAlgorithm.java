/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph.alg;

import analyzer.PatternOccurrence;
import graph.*;

import java.util.*;

/**
 * Created by Tom Warnke.
 */
public class UllmannsAlgorithm {

    private final Graph dataGraph;
    private final Graph patternGraph;
    private final Set<PatternOccurrence> matches;
    private final List<Node> matchingOrder;

    public UllmannsAlgorithm(Graph dataGraph, Graph patternGraph) {
        this.dataGraph = dataGraph;
        this.patternGraph = patternGraph;
        matches = new HashSet<>();

        matchingOrder = determineMatchingOrder();
    }

    private List<Node> determineMatchingOrder() {

        List<Node> matchingOrder = new ArrayList<>(patternGraph.getNodes().size());

        Set<Edge> borderEdges = new HashSet<>();

        for (int i = 0; i < patternGraph.getNodes().size(); i++) {
            Node nodeToMatch = selectNextNodeToMatch(matchingOrder, borderEdges);
            matchingOrder.add(i, nodeToMatch);
            updateBorderEdges(nodeToMatch, borderEdges);
        }

        return matchingOrder;
    }

    public Set<PatternOccurrence> getAllMatches() {
        matchNextNode(new HashMap<>());
        return matches;
    }

    private void matchNextNode(Map<Node, Node> mapping) {

        if (mapping.size() == patternGraph.getNodes().size()) {
            // all nodes matched
            PatternOccurrence match = new PatternOccurrence();
            for (Map.Entry<Node, Node> entry : mapping.entrySet()) {
                match.put(entry.getKey().getAttribute("id").toString(),
                        entry.getValue().getAttribute("id").toString());
            }
            matches.add(match);
            if (matches.size() % 100 == 0)
                System.out.println("Found " + matches.size() + " results so far.");
        } else {
            // some nodes are left to match => get next node to match from predefined matching order
            Node nextNodeToMatch = matchingOrder.get(mapping.size());
            Set<Node> candidates = determineCandidates(mapping, nextNodeToMatch);

            //            String output = new String(new char[mapping.size()]).replace("\0", "| ");
            //            System.out.println(output + "Matching " + candidates.size() + " candidates for " +
            // nextNodeToMatch.getId());

            for (Node candidate : candidates) {
                // second candidate will override first etc.
                mapping.put(nextNodeToMatch, candidate);
                matchNextNode(mapping);
            }
            // remove this pattern node for backtracking
            mapping.remove(nextNodeToMatch);
        }
    }

    private Set<Node> determineCandidates(Map<Node, Node> mapping, Node patternNodeToMatch) {
        // edges in the pattern and the nodes they lead to
        Map<Edge, Node> matchedPatternNeighbours = new HashMap<>();

        for (Edge inEdge : patternNodeToMatch.getInEdges()) {
            if (mapping.keySet().contains(inEdge.getStart())) {
                matchedPatternNeighbours.put(inEdge, inEdge.getStart());
            }
        }
        for (Edge outEdge : patternNodeToMatch.getOutEdges()) {
            if (mapping.keySet().contains(outEdge.getEnd())) {
                matchedPatternNeighbours.put(outEdge, outEdge.getEnd());
            }
        }

        if (matchedPatternNeighbours.isEmpty()) {
            // just get all unmatched data nodes of the correct type - we can not do better
            // this will happen to match the very first pattern node, for example
            Set<Node> result = new HashSet<>(dataGraph.getNodes(patternNodeToMatch.getType()));
            result.removeAll(mapping.values());
            return result;
        } else {
            // we get the data nodes for the already matched pattern nodes, and follow the edges from them to get a
            // set of candidates each and then compute the intersection to get the overall candidate set
            Set<Node> result = getCandidateIntersection(patternNodeToMatch, matchedPatternNeighbours, mapping);
            return result;
        }
    }

    private Set<Node> getCandidateIntersection(Node patternNodeToMatch, Map<Edge, Node> matchedPatternNeighbours,
                                               Map<Node, Node> mapping) {

        Set<Node> result = new HashSet<>();

        for (Map.Entry<Edge, Node> matchedPatternNeighbour : matchedPatternNeighbours.entrySet()) {

            Node matchedDataNode = mapping.get(matchedPatternNeighbour.getValue());
            Set<Node> candidates = getCandidatesFromPatternNeighbour(patternNodeToMatch, matchedPatternNeighbour
                            .getKey(),
                    matchedPatternNeighbour.getValue(), matchedDataNode);

            if (candidates.isEmpty()) {
                // if one candidate set is empty, we can already stop
                return candidates;
            } else {
                // we found candidates
                if (result.isEmpty()) {
                    // first candidates
                    result.addAll(candidates);
                } else {
                    // intersect candidate sets
                    result.retainAll(candidates);
                    if (result.isEmpty()) {
                        // got an empty intersection => makes no sense to continue
                        return result;
                    }
                }
            }
        }
        return result;
    }

    private Set<Node> getCandidatesFromPatternNeighbour(Node patternNodeToMatch, Edge neighbourEdge, Node
            patternNeighbour, Node dataNeighbour) {

        Set<Node> candidates = new HashSet<>();

        if (neighbourEdge.getStart() == patternNeighbour) {
            // starts at already matched node
            for (Edge edge : dataNeighbour.getOutEdges()) {
                if (edge.getType() == neighbourEdge.getType()) {
                    Object typeToMatch = patternNodeToMatch.getAttribute("type");
                    if (typeToMatch != null) {
                        if (typeToMatch.equals(edge.getEnd().getType())) {
                            candidates.add(edge.getEnd());
                        }
                    } else {
                        candidates.add(edge.getEnd());
                    }
                }
            }
        } else {
            // ends at already matched node
            for (Edge edge : dataNeighbour.getInEdges()) {
                if (edge.getType() == neighbourEdge.getType()) {
                    Object typeToMatch = patternNodeToMatch.getAttribute("type");
                    if (typeToMatch != null) {
                        if (typeToMatch.equals(edge.getEnd().getType())) {
                            candidates.add(edge.getStart());
                        }
                    } else {
                        candidates.add(edge.getStart());
                    }
                }
            }
        }
        return candidates;
    }

    private Node selectNextNodeToMatch(List<Node> matchingOrder, Set<Edge> borderEdges) {
        if (borderEdges.isEmpty()) {
            Set<Node> unmatchedPatternNodes = new HashSet<>(patternGraph
                    .getNodes());
            unmatchedPatternNodes.removeAll(matchingOrder);
            if (unmatchedPatternNodes.isEmpty()) {
                // this should never happen
                throw new IllegalStateException();
            }
            Node objectNode = null, locationNode = null;
            // search through nodes
            for (Node unmatchedPatternNode : unmatchedPatternNodes) {
                if (unmatchedPatternNode.getType() == NodeType.TIMEPOINT) {
                    // prefer time point node
                    return unmatchedPatternNode;
                } else if (objectNode == null && unmatchedPatternNode.getType() == NodeType.OBJECT) {
                    objectNode = unmatchedPatternNode;
                } else if (locationNode == null && unmatchedPatternNode.getType() == NodeType.LOCATION) {
                    locationNode = unmatchedPatternNode;
                }
            }
            // found no time point node
            if (objectNode != null) {
                // at least an object node
                return objectNode;
            } else {
                return locationNode;
            }
        } else {
            // some border edges exist
            // we want to match object nodes and time point nodes first, location nodes last
            Node timePointNode = null, objectNode = null, objectNodeFromObjectNode = null, locationNodeFromObjectNode =
                    null,
                    locationNodeFromTimePointNode = null, locationNodeFromLocationNode = null;
            for (Edge borderEdge : borderEdges) {
                if (borderEdge.getType() == EdgeType.OBJECT_TO_LOCATION) {
                    if (matchingOrder.contains(borderEdge.getStart())) {
                        // edge from object to location
                        if (locationNodeFromObjectNode == null) {
                            locationNodeFromObjectNode = borderEdge.getEnd();
                        }
                    } else if (matchingOrder.contains(borderEdge.getEnd())) {
                        // edge from location to object
                        if (objectNode == null)
                            objectNode = borderEdge.getStart();
                    }
                } else if (borderEdge.getType() == EdgeType.TIMEPOINT_TO_LOCATION) {
                    if (matchingOrder.contains(borderEdge.getStart())) {
                        // edge from time point to location
                        if (locationNodeFromTimePointNode == null) {
                            locationNodeFromTimePointNode = borderEdge.getEnd();
                        }
                    } else if (matchingOrder.contains(borderEdge.getEnd())) {
                        // edge from location to time point
                        if (timePointNode == null) {
                            timePointNode = borderEdge.getStart();
                        }
                    }
                } else if (borderEdge.getType() == EdgeType.NEXT_TIMEPOINT) {
                    // edge from time point to next time point - edge direction does not matter
                    if (timePointNode == null) {
                        if (matchingOrder.contains(borderEdge.getStart())) {
                            timePointNode = borderEdge.getEnd();
                        } else if (matchingOrder.contains(borderEdge.getEnd())) {
                            timePointNode = borderEdge.getStart();
                        }
                    }
                } else if (borderEdge.getType() == EdgeType.SAME_TEAM) {
                    // edge from object to object - edge direction does not matter
                    if (objectNodeFromObjectNode == null) {
                        if (matchingOrder.contains(borderEdge.getStart())) {
                            objectNodeFromObjectNode = borderEdge.getEnd();
                        } else if (matchingOrder.contains(borderEdge.getEnd())) {
                            objectNodeFromObjectNode = borderEdge.getStart();
                        }
                    }
                } else {
                    // is a direction edge between locations - edge direction does not matter
                    if (locationNodeFromLocationNode == null) {
                        if (matchingOrder.contains(borderEdge.getStart())) {
                            locationNodeFromLocationNode = borderEdge.getEnd();
                        } else if (matchingOrder.contains(borderEdge.getEnd())) {
                            locationNodeFromLocationNode = borderEdge.getStart();
                        }
                    }
                }
            }
            if (locationNodeFromLocationNode != null) {
                return locationNodeFromLocationNode;
            }
            if (locationNodeFromTimePointNode != null) {
                return locationNodeFromTimePointNode;
            }
            if (objectNode != null) {
                return objectNode;
            }
            if (timePointNode != null) {
                return timePointNode;
            }
            if (objectNodeFromObjectNode != null) {
                return objectNodeFromObjectNode;
            }
            if (locationNodeFromObjectNode != null) {
                return locationNodeFromObjectNode;
            }
            throw new IllegalStateException();
        }
    }

    private void updateBorderEdges(Node nodeToMatch, Set<Edge> borderEdges) {
        // remove edges that are in the border edge set, add edges that are not

        for (Edge inEdge : nodeToMatch.getInEdges()) {
            if (borderEdges.contains(inEdge)) {
                borderEdges.remove(inEdge);
            } else {
                borderEdges.add(inEdge);
            }
        }
        for (Edge outEdge : nodeToMatch.getOutEdges()) {
            if (borderEdges.contains(outEdge)) {
                borderEdges.remove(outEdge);
            } else {
                borderEdges.add(outEdge);
            }
        }
    }
}
