/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph;

import org.apache.log4j.Logger;
import robocup.server.connection.message.IRoboCupMessageHandler;
import robocup.server.connection.message.RoboCupShowMessage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Tom
 */
public class MemoryGraphMessageHandler implements IRoboCupMessageHandler {

    private static final Logger log =
            Logger.getLogger(MemoryGraphMessageHandler.class);

    private final Graph graph = new GraphImpl();

    private boolean firstMessage = true;

    public Graph getGraph() {
        return graph;
    }

    @Override
    public void handleMessage(RoboCupShowMessage message) {
        // first turn, insert object nodes
        if (firstMessage) {
            addObjectNodes(message.getLocations().keySet());
            firstMessage = false;
            log.debug("Inserted object nodes");
        }
        addTurnData(message);
        log.debug("Finished processing turn " + message.getTurn());
    }

    private void addObjectNodes(Collection<String> ids) {

        List<Node> leftPlayers = new ArrayList<>(11);
        List<Node> rightPlayers = new ArrayList<>(11);

        for (String id : ids) {

            Node object = graph.newNode(NodeType.OBJECT, id);
            object.setAttribute("id", id);
            if (id.equals("ball")) {
                object.setAttribute("type", "ball");
            } else {
                object.setAttribute("type", "player");
                if (id.startsWith("l")) {
                    object.setAttribute("team", "l");
                    leftPlayers.add(object);
                } else if (id.startsWith("r")) {
                    object.setAttribute("team", "r");
                    rightPlayers.add(object);
                }
            }
        }

        // insert same team edges and opposite team edges
        for (Node player1 : leftPlayers) {
            for (Node player2 : leftPlayers) {
                if (player1 != player2) {
                    graph.newEdge(EdgeType.SAME_TEAM, player1, player2);
                }
            }
            for (Node player2 : rightPlayers) {
                graph.newEdge(EdgeType.OPPOSITE_TEAM, player1, player2);
            }
        }

        for (Node player1 : rightPlayers) {
            for (Node player2 : rightPlayers) {
                if (player1 != player2) {
                    graph.newEdge(EdgeType.SAME_TEAM, player1, player2);
                }
            }
            for (Node player2 : leftPlayers) {
                graph.newEdge(EdgeType.OPPOSITE_TEAM, player1, player2);
            }
        }

    }

    private void addTurnData(RoboCupShowMessage message) {

        String turn = String.valueOf(message.getTurn());

        if (graph.getNode(NodeType.TIMEPOINT, turn) != null) {
            // observation for this turn exist already
            return;
        }

        Node turnNode = graph.newNode(NodeType.TIMEPOINT, turn);

        Node previousTurnNode = graph
                .getNode(NodeType.TIMEPOINT, String.valueOf(message.getTurn()
                        - 1));

        if (previousTurnNode != null) {
            graph.newEdge(EdgeType.NEXT_TIMEPOINT, previousTurnNode, turnNode);
        }

        List<Node> locationNodes = new ArrayList<>(23);

        message.getLocations().forEach((id, location) -> {
            Node locationNode = graph.newNode(NodeType.LOCATION, id + "_" + turn);
            locationNode.setAttribute("x", location.getX());
            locationNode.setAttribute("y", location.getY());
            graph.newEdge(EdgeType.TIMEPOINT_TO_LOCATION, turnNode,
                    locationNode);

            Node objectNode = graph.getNode(NodeType.OBJECT, id);
            graph.newEdge(EdgeType.OBJECT_TO_LOCATION, objectNode,
                    locationNode);

            locationNodes.add(locationNode);
        });

        // compute direction relation and insert edges between nodes
        CustomGraphDirectionRelation relation = new
                CustomGraphDirectionRelation();

        for (Node firstLocation : locationNodes) {
            for (Node secondLocation : locationNodes) {
                if (firstLocation != secondLocation) {
                    double opponentGoalX;
                    if (firstLocation.getId().startsWith("l")) {
                        opponentGoalX = 50.0;
                    } else if (firstLocation.getId().startsWith("r")) {
                        opponentGoalX = -50.0;
                    } else {
                        // must be the ball, no player
                        continue;
                    }
                    EdgeType direction =
                            relation.computeOnLocationPair(firstLocation, secondLocation, opponentGoalX < 0.0);
                    graph.newEdge(direction, firstLocation, secondLocation);

                }
            }
        }
    }
}
