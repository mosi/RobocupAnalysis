/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph;

/**
 * Created by Tom
 */
class EdgeImpl implements Edge {

    private final EdgeType type;

    private final Node start, end;

    public EdgeImpl(EdgeType type, Node start, Node end) {
        this.type = type;
        this.start = start;
        this.end = end;
        start.addOutEdge(this);
        end.addInEdge(this);
    }

    public static Edge fromString(String string, Graph graph) {

        if (string.startsWith("Edge={") && string.endsWith("}")) {

            String[] data = string.substring(6, string.length() - 1).split(",");

            if (data.length >= 5) {

                Node start = graph.getNode(NodeType.valueOf(data[0]), data[1]);
                Node end = graph.getNode(NodeType.valueOf(data[2]), data[3]);
                EdgeType type = EdgeType.valueOf(data[4]);

                Edge edge = graph.newEdge(type, start, end);

                return edge;
            }
        }

        return null;

    }

    @Override
    public EdgeType getType() {
        return type;
    }

    @Override
    public Node getStart() {
        return start;
    }

    @Override
    public Node getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        EdgeImpl edge = (EdgeImpl) o;

        if (!type.equals(edge.type))
            return false;
        if (!start.equals(edge.start))
            return false;
        return end.equals(edge.end);

    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + start.hashCode();
        result = 31 * result + end.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("Edge={").append(start.getType()).append(',')
                .append(start.getId()).append(',').append(end.getType()).append(',')
                .append(end.getId()).append(',').append(type).append('}');

        return builder.toString();
    }
}
