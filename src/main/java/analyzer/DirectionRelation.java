/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package analyzer;

/**
 * Created by tw250
 */
public class DirectionRelation {

    public enum Direction {
        CLOSE, FRONT, FRONT_LEFT, LEFT, REAR_LEFT, REAR, REAR_RIGHT, RIGHT, FRONT_RIGHT, FARAWAY
    }

    private final double threshold = Math.tan(Math.PI / 8);

    private final double maxDist = 25;
    private final double minDist = 5;

    public Direction computeOnLocationPair(double dX, double dY, boolean invert) {

        if (invert) {
            dX = -dX;
            dY = -dY;
        }

        if (Math.sqrt(dX * dX + dY * dY) <= minDist) {
            return Direction.CLOSE;
        }
        if (Math.sqrt(dX * dX + dY * dY) >= maxDist) {
            return Direction.FARAWAY;
        }

        if (threshold * Math.abs(dY) >= Math.abs(dX)) {
            if (dY > 0) {
                return Direction.RIGHT;
            } else {
                return Direction.LEFT;
            }
        } else if (threshold * Math.abs(dX) >= Math.abs(dY)) {
            if (dX > 0) {
                return Direction.FRONT;
            } else {
                return Direction.REAR;
            }
        } else if (dX > 0 && dY > 0) {
            return Direction.FRONT_RIGHT;
        } else if (dX > 0 && dY < 0) {
            return Direction.FRONT_LEFT;
        } else if (dX < 0 && dY > 0) {
            return Direction.REAR_RIGHT;
        } else if (dX < 0 && dY < 0) {
            return Direction.REAR_LEFT;
        }

        throw new IllegalStateException();
    }

}
