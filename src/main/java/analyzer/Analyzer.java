/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package analyzer;

import graph.*;
import graph.alg.UllmannsAlgorithm;
import neo4j.Neo4jGraphMessageHandler;
import neo4j.Neo4jGraphQueryManager;
import org.apache.log4j.Logger;
import robocup.server.RoboCupSoccerServerAdapter;
import robocup.server.connection.ISoccerServerConnection;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Tom Warnke
 */
public class Analyzer {

    private static final Logger log = Logger.getLogger(Analyzer.class);

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

                runQuery();

//        observeAndStoreMatch();

    }

    private static void observeAndStoreMatch() throws IOException {
        boolean autoMode = true;

        log.info("Starting server...");

        ISoccerServerConnection connection = RoboCupSoccerServerAdapter
                .setUpLocalServer("/home/tom/Robocup/AutoMatch", autoMode);

        log.info("Observing...");

        Neo4jGraphMessageHandler neoHandler = new Neo4jGraphMessageHandler("/home/tom/Robocup/paper/neo1");
        MemoryGraphMessageHandler customHandler = new MemoryGraphMessageHandler();

        connection.registerMessageHandler(neoHandler);
        connection.registerMessageHandler(customHandler);
        connection.observe(autoMode);

        Graph graph = customHandler.getGraph();
        GraphPersistence.writeGraph(graph, "/home/tom/Robocup/paper/custom1");

        log.info("Done!");
    }

    private static Graph loadMatch(String dbPath) {

        return GraphPersistence.readGraph(dbPath);

    }

    private static String generateFolderName() {

        DateFormat dateFormat = new SimpleDateFormat("yyMMdd-HHmmss");
        Date date = new Date();
        return "robocup-" + dateFormat.format(date);

    }

    private static void runQuery() {

        long before, after;

        String queryString = getBackFourPatternCypher(5);
//        String queryString = getPastDefenderPatternCypher();

        Neo4jGraphQueryManager manager = new Neo4jGraphQueryManager("/home/tom/Robocup/paper/neo1");

        before = System.currentTimeMillis();
        Set<PatternOccurrence> neoPatternOccurrences = manager.runQuery(queryString);
        after = System.currentTimeMillis();
        System.out.println("Neo4j Query took " + (after - before) + "ms and returned " + neoPatternOccurrences.size()
                + " results.");

        Graph dataGraph = GraphPersistence.readGraph("/home/tom/Robocup/paper/custom1");
        Graph patternGraph = getBackFourPatternGraph(5);
        UllmannsAlgorithm ullmann = new UllmannsAlgorithm(dataGraph, patternGraph);

        before = System.currentTimeMillis();
        Set<PatternOccurrence> ullmannPatternOccurrences = ullmann.getAllMatches();
        after = System.currentTimeMillis();
        System.out.println("Ullmann's algorithm took " + (after - before) + "ms and returned " +
                ullmannPatternOccurrences.size() + " results.");

        return;

    }

    private static Graph getBackFourPatternGraph(int duration) {

        Graph pattern = GraphManager.emptyGraph();

        Node[] timePoints = new Node[duration];
        for (int i = 0; i < timePoints.length; i++) {
            timePoints[i] = pattern.newNode(NodeType.TIMEPOINT, "t" + i);
        }
        for (int i = 0; i < timePoints.length - 1; i++) {
            pattern.newEdge(EdgeType.NEXT_TIMEPOINT, timePoints[i], timePoints[i + 1]);
        }

        Node[] objects = new Node[4];
        for (int i = 0; i < objects.length; i++) {
            objects[i] = pattern.newNode(NodeType.OBJECT, "o" + i);
        }
        for (int i = 0; i < objects.length - 1; i++) {
            pattern.newEdge(EdgeType.SAME_TEAM, objects[i], objects[i + 1]);
        }


        for (int i = 0; i < duration; i++) {
            Node[] locations = new Node[objects.length];
            for (int j = 0; j < objects.length; j++) {
                locations[j] = pattern.newNode(NodeType.LOCATION, "l" + i + "_" + j);
                pattern.newEdge(EdgeType.OBJECT_TO_LOCATION, objects[j], locations[j]);
                pattern.newEdge(EdgeType.TIMEPOINT_TO_LOCATION, timePoints[i], locations[j]);
            }
            for (int j = 0; j < locations.length - 1; j++) {
                pattern.newEdge(EdgeType.LEFT, locations[j], locations[j + 1]);
            }
        }

        return pattern;
    }

    private static String getBackFourPatternCypher(int duration) {

        String queryString =
                "MATCH (l1:LOCATION)-[:LEFT]->(l2:LOCATION)-[:LEFT]->(l3:LOCATION)-[:LEFT]->(l4:LOCATION), " +
                        "(l1)--(one:OBJECT), (l2)--(two:OBJECT), (l3)--(three:OBJECT), (l4)--(four:OBJECT), " +
                        "(l1)--(t:TIME_POINT) " +
                        "WHERE one.team = two.team AND two.team = three.team AND three.team = four.team " +
                        "WITH one, two, three, four, collect(t) AS times " +
                        "MATCH path=(start:TIME_POINT)-[:NEXT_TIME_POINT*" + (duration - 1) + "]->(end:TIME_POINT) " +
                        "WHERE ALL (t in nodes(path) WHERE t in times) " +
                        "RETURN one.id, two.id, three.id, four.id, start.time";
        return queryString;
    }

    private static String getPastDefenderPatternCypher() {

        String queryString =
                "MATCH (a:LOCATION)-[:CLOSE]->(b:LOCATION), (a)-[:FRONT]->(d:LOCATION)," +
                        "(a)--(attacker:OBJECT), (b)--(ball:OBJECT), (d)--(defender:OBJECT), " +
                        "(a)--(t:TIME_POINT) " +
                        "WHERE ball.type = \"ball\" AND NOT attacker.team = defender.team " +
                        "WITH attacker, defender, collect(t) AS before_times " +
                        "MATCH (a:LOCATION)-[:CLOSE]->(b:LOCATION), (a)-[:REAR]->(d:LOCATION)," +
                        "(a)--(attacker:OBJECT), (b)--(ball:OBJECT), (d)--(defender:OBJECT), " +
                        "(a)--(t:TIME_POINT) " +
                        "WITH attacker, defender, before_times, collect(t) AS after_times " +
                        "MATCH path=(start:TIME_POINT)-[:NEXT_TIME_POINT*100]->(end:TIME_POINT) " +
                        "WHERE start in before_times AND ANY (t in nodes(path) WHERE end in after_times) " +
                        "RETURN attacker.id, defender.id, start.time, end.time";
        return queryString;
    }
}
