/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neo4j;

import robocup.server.connection.message.IRoboCupMessageHandler;
import robocup.server.connection.message.RoboCupShowMessage;

/**
 * This class receives observation messages from robocup and dispatches them
 * to the database connector.
 *
 * @author Tom Warnke.
 */
public class Neo4jGraphMessageHandler implements IRoboCupMessageHandler {

    private final Neo4jGraphDatabaseConnector connector;

    public Neo4jGraphMessageHandler(String dbPath) {
        this.connector = new Neo4jGraphDatabaseConnector(dbPath);
    }

    @Override
    public void handleMessage(RoboCupShowMessage message) {
        connector.addTurnData(message);
    }


}
