/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neo4j;

import analyzer.PatternOccurrence;
import org.apache.log4j.Logger;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import robocup.server.connection.message.RoboCupShowMessage;

import java.io.File;
import java.util.*;

/**
 * Created by Tom Warnke.
 */
class Neo4jGraphDatabaseConnector {

    private static final Logger log =
            Logger.getLogger(Neo4jGraphDatabaseConnector.class);

    private final GraphDatabaseService graphDb;

    private final Neo4jDirectionRelation relation = new
            Neo4jDirectionRelation();

    private boolean firstMessage = true;

    public Neo4jGraphDatabaseConnector(String dbPath) {

        File dbFile = new File(dbPath);
        graphDb = new
                GraphDatabaseFactory().newEmbeddedDatabaseBuilder(dbFile)
                .newGraphDatabase();
        registerShutdownHook(graphDb);
    }

    public void addTurnData(RoboCupShowMessage message) {
        // first turn, insert object nodes
        if (firstMessage) {
            addObjectNodes(message.getLocations().keySet());
            firstMessage = false;
            log.debug("Inserted object nodes");
        }
        addLocationNodes(message);
        log.debug("Finished processing turn " + message.getTurn());
    }

    private void addLocationNodes(RoboCupShowMessage message) {

        try (Transaction tx = graphDb.beginTx()) {

            if (graphDb.findNode(NodeType.TIME_POINT,
                    "time", message.getTurn()) == null) {
                // no nodes for this turn exist

                Node timePointNode = graphDb.createNode(NodeType.TIME_POINT);
                timePointNode.setProperty("time", message.getTurn());

                Node previousTimePointNode = graphDb.findNode
                        (NodeType.TIME_POINT, "time", message.getTurn() - 1);
                if (previousTimePointNode != null) {
                    previousTimePointNode.createRelationshipTo(timePointNode,
                            GraphRelationshipType.NEXT_TIME_POINT);
                }

                Map<Node, Node> locationNodes = new HashMap<>(23);

                message.getLocations().forEach((id, location) -> {
                    Node locationNode = graphDb.createNode(NodeType.LOCATION);
                    locationNode.setProperty("x", location.getX());
                    locationNode.setProperty("y", location.getY());

                    timePointNode.createRelationshipTo(locationNode,
                            GraphRelationshipType.TIMEPOINT_TO_LOCATION);

                    Node objectNode = graphDb.findNode(NodeType.OBJECT,
                            "id", id);
                    if (objectNode != null) {
                        objectNode.createRelationshipTo(locationNode,
                                GraphRelationshipType.OBJECT_TO_LOCATION);
                    }

                    locationNodes.put(locationNode, objectNode);
                });

                for (Node firstLocation : locationNodes.keySet()) {
                    for (Node secondLocation : locationNodes.keySet()) {
                        if (firstLocation != secondLocation) {
                            double opponentGoalX;
                            Node firstObject = locationNodes.get(firstLocation);
                            Node secondObject = locationNodes.get(secondLocation);

                            if (firstObject.hasProperty("team") && firstObject.getProperty("team").equals("l")) {
                                opponentGoalX = 50.0;
                            } else if (firstObject.hasProperty("team") && firstObject.getProperty("team").equals("r")) {
                                opponentGoalX = -50.0;
                            } else {
                                // must be the ball, no player
                                continue;
                            }

                            SpatialRelationshipType direction =
                                    relation.computeOnLocationPair(firstLocation, secondLocation, opponentGoalX < 0.0);
                            firstLocation.createRelationshipTo(secondLocation,
                                    direction);

                        }
                    }
                }
            }

            tx.success();
        }
    }

    private void addObjectNodes(Set<String> ids) {

        try (Transaction tx = graphDb.beginTx()) {
            for (String id : ids) {
                Node objectNode = graphDb.createNode(NodeType.OBJECT);
                objectNode.setProperty("id", id);
                if (id.equals("ball")) {
                    objectNode.setProperty("type", "ball");
                } else {
                    objectNode.setProperty("type", "player");
                    if (id.startsWith("l")) {
                        objectNode.setProperty("team", "l");
                    } else if (id.startsWith("r")) {
                        objectNode.setProperty("team", "r");
                    }
                }
            }
            tx.success();
        }
    }

    public Set<PatternOccurrence> runQuery(String queryString, Map<String,
            Object> parameters) {

        Set<PatternOccurrence> patternOccurrences = new HashSet<>();

        try (Transaction tx = graphDb.beginTx()) {

            Result result = graphDb.execute(queryString, parameters);

            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                System.out.println(row);
                PatternOccurrence patternOccurrence = new PatternOccurrence
                        (row);
                patternOccurrences.add(patternOccurrence);
            }

            result.close();

            tx.success();
        }

        return patternOccurrences;

    }

    private static void registerShutdownHook(final GraphDatabaseService
                                                     graphDb) {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                graphDb.shutdown();
            }
        });
    }

}
