/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neo4j;

import analyzer.DirectionRelation;
import org.neo4j.graphdb.Node;

/**
 * Created by Tom Warnke.
 */
public class Neo4jDirectionRelation extends DirectionRelation {

    public SpatialRelationshipType computeOnLocationPair(Node first, Node
            second, boolean invert) {

        float dX = 0, dY = 0;

        dX = (float) second.getProperty("x") - (float) first.getProperty("x");
        dY = (float) second.getProperty("y") - (float) first.getProperty("y");

        Direction direction = computeOnLocationPair(dX, dY, invert);

        return SpatialRelationshipType.valueOf(direction.name());
    }
}
