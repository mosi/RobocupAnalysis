/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package graph;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Tom
 */
public class GraphImplTest {

    @Test
    public void createGraph() {

        Graph graph = new GraphImpl();

        Node node1 = graph.newNode(NodeType.OBJECT, "n1");
        Node node2 = graph.newNode(NodeType.LOCATION, "n2");
        Node node3 = graph.newNode(NodeType.LOCATION, "n3");

        Edge edge1 = graph.newEdge(EdgeType.OBJECT_TO_LOCATION, node1, node2);
        Edge edge2 = graph.newEdge(EdgeType.OBJECT_TO_LOCATION, node1, node3);

        assertEquals(edge1.getStart(), node1);
        assertEquals(edge1.getEnd(), node2);
        assertTrue(node1.getOutEdges().contains(edge1));
        assertTrue(node2.getInEdges().contains(edge1));

    }

    @Test
    public void serializeGraph() {

        String file = "graphtest.ser";

        Graph graphToWrite = new GraphImpl();

        Node node1 = graphToWrite.newNode(NodeType.OBJECT, "n1");
        node1.setAttribute("type", "player");
        node1.setAttribute("team", "l");
        Node node2 = graphToWrite.newNode(NodeType.LOCATION, "n2");
        Node node3 = graphToWrite.newNode(NodeType.LOCATION, "n3");

        Edge edge1 =
                graphToWrite.newEdge(EdgeType.OBJECT_TO_LOCATION, node1, node2);
        Edge edge2 =
                graphToWrite.newEdge(EdgeType.OBJECT_TO_LOCATION, node1, node3);

        GraphPersistence.writeGraph(graphToWrite, file);

        Graph readGraph = GraphPersistence.readGraph(file);

        System.out.println(readGraph.toString());

    }

}
