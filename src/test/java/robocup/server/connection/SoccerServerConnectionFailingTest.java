/*
 * Copyright 2016 Tom Warnke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package robocup.server.connection;

import org.junit.Test;
import robocup.server.connection.message.DefaultRoboCupMessageHandler;

import java.io.IOException;

/**
 * @author Tom Warnke
 */
public class SoccerServerConnectionFailingTest {

    /**
     * Test method for {@link ISoccerServerConnection#observe(boolean)}. Test a connection to a soccer server instance
     * running on localhost. It must be ready (i.e. teams must be on the pitch) before this test is started.
     */
    //@Test
    public void testObserve() {

        boolean autoMode = true;

        try {
            SoccerServerConnection soccerServerConnection =
                    SoccerServerConnectionManager.getConnectionTo
                            ("localhost", 6000);
            soccerServerConnection.registerMessageHandler(new DefaultRoboCupMessageHandler());
            soccerServerConnection.observe(autoMode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
